package com.mitocode.model;


import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table (name="venta")

public class Venta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idVenta;
	
	@ManyToOne
	@JoinColumn(name="idPersona",nullable=false)
	private Persona persona;
		
	@JsonSerialize (using=ToStringSerializer.class)
	private LocalDateTime fecha;
	
	@Column(name="importe",nullable=false)
	private Double importe;
	
	//MAESTRO DETALLE
	//LAZY = traer solo select de la tabla Venta, más no DETALLE venta xk puede tener muchos registros
	//PERSIST,MERGE,REMOVE=para la hora que hagamos un select o un delete, se borre tanto de la tabla VENTA como DETALLEVENTA
	@OneToMany(mappedBy ="venta", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<DetalleVenta> detalleventa;

	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public List<DetalleVenta> getDetalleventa() {
		return detalleventa;
	}

	public void setDetalleventa(List<DetalleVenta> detalleventa) {
		this.detalleventa = detalleventa;
	}
	
	
}
