package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private IProductoService service;
	
	@PostMapping(value="/registrar", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Integer> registrar(@RequestBody Producto producto){
		int resultado=0;
		try {
			service.registrar(producto);
			resultado = 1;
		}
		catch(Exception e) {
			resultado = 0;
		}
		
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
	}

	
	@PutMapping(value="/actualizar", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Integer> modifcar(@RequestBody Producto producto){
		int resultado=0;
		try {
			service.modificar(producto);
			resultado=1;
		}catch (Exception e) {
			// TODO: handle exception
			resultado=0;
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
		
	}
	
	@DeleteMapping(value="/eliminar/{idProducto}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable Integer idProducto){
		int resultado=0;
		try {
			service.eliminar(idProducto);
			resultado=1;
		}catch (Exception e) {
			// TODO: handle exception
			resultado=0;
		}
		return new ResponseEntity<Integer>(resultado,HttpStatus.OK);
		
	}
		
	@GetMapping(value="/listar", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> productos= new ArrayList<>();
		productos= service.listar();
		return new ResponseEntity<List<Producto>>(productos,HttpStatus.OK);
	}
	
	@GetMapping(value="/listar/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> listarId(@PathVariable("id") Integer idProducto){
		Producto producto=new Producto();
		producto=service.listarId(idProducto);
		return new ResponseEntity<Producto>(producto,HttpStatus.OK);
		
	}
	
	
	
	
}
